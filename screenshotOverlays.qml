import QtQuick 2.0
import QtQuick.Layouts 1.1

import org.kde.plasma.core 2.0 as PlasmaCore

// This tool makes it easy to add overlays onto images for use in screenshots
// currently it overlays "underconstruction" or splits the view in two and adds before/after respectively

// To use modify the properties at the start of this class
// then take a screenshot with ksnapshot + hide window/borders

Image {

    //BEGIN THINGS TO CHANGE
    
    width: 840
    height: width/1.6 //golden ratio

    //valid modes are "beforeafter" or "underconstruction"
    property string mode: "beforeafter"
    property string image1Source: "screenshot.png"
    property string image2Source: "/home/david/ktp_group_after.png"

    //END THINGS TO CHANGE

    source: "assets/background.png"
    fillMode: Image.Tile

    RowLayout {
        anchors.fill: parent
        spacing: 0

        View {
            imageSource: image1Source
            overlayType: mode == "underconstruction" ? "underconstruction" : "before"

            Layout.fillWidth: true
            Layout.fillHeight: true
        }
        View {
            visible: mode == "beforeafter"
            imageSource: image2Source
            overlayType: "after"

            Layout.fillWidth: true
            Layout.fillHeight: true
        }
    }
}